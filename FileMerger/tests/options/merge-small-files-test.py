###################
# Define InputData
###################

from PRConfig import TestFileDB
TestFileDB.test_file_db["R08S14_smallfiles"].run()

from Configurables import FileMerger
FileMerger().OutputFile="MergedSmallFiles.dst"
##############################################
#Debug printout, lists all cleaned directories
##############################################

from Configurables import FSRCleaner
FSRCleaner().OutputLevel=2
#FSRCleaner().Enable=False

##############################
#fill summary every event
##############################

from Configurables import LHCbApp
LHCbApp().XMLSummary="summary.xml"
from Configurables import XMLSummarySvc
XMLSummarySvc("CounterSummarySvc").UpdateFreq=1
