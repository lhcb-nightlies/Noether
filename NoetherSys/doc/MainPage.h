/** \mainpage notitle
 *  \anchor noetherdoxygenmain
 *
 * This reference manual documents all %LHCb and %Gaudi classes accessible from
 * the Noether applications environment. More information on Noether
 * applications, including a list of available releases, can be found in the 
 * <a href=" http://cern.ch/lhcb-release-area/DOC/noether/">Noether project Web pages</a>
 *
 * <hr>
 * \htmlinclude new_release.notes

 *
 */
